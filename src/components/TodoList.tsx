import React from "react";
import { Droppable } from "react-beautiful-dnd";
import { Todo } from "../model";
import "../style.css";
import SingleTodo from "./SingleTodo";
interface props {
  todos: Array<Todo>;
  setTodos: React.Dispatch<React.SetStateAction<Array<Todo>>>;
  completed: Array<Todo>;
  setCompletedTodos:React.Dispatch<React.SetStateAction<Array<Todo>>>
}
const TodoList: React.FC<props> = ({ todos, setTodos ,completed,setCompletedTodos}: props) => {
  return (
    
   <div className="container">
     <Droppable droppableId="TodoList">

       {(provided)=>(
         <div className="todos" ref={provided.innerRef}{...provided.droppableProps}>
         <span>Active Task</span>
         {todos.map((todo,index)=>(
          <SingleTodo
          index={index}
          todo={todo}
          key={todo.id}
          todos={todos}
          setTodos={setTodos}
          />
         ))} 
       {provided.placeholder}
    </div>
       )}
     </Droppable>
     <Droppable droppableId="TodoRemove">
       {(provided)=>(
         <div className="todoremove" ref={provided.innerRef}{...provided.droppableProps}>
         <span>Completed Task</span>
         {completed.map((todo,index)=>(
         <SingleTodo
         index={index}
         todo={todo}
         key={todo.id}
         todos={completed}
         setTodos={setCompletedTodos}
         />
        ))}
         {provided.placeholder} 
      </div>
       )}
     </Droppable>
     
</div>
  );
};

export default TodoList;
